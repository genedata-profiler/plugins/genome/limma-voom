#########################################################
###                  Summarize                        ###
#########################################################

shared.dir <- Sys.getenv("shared_folder")
index.file <- paste0(shared.dir, "/html/index.html")
version.file <- paste0(shared.dir, "/version_info.txt")

version_info <- paste0("R version ", getRversion(), ", ", "limma version ", packageVersion("limma"), ", ", "edgeR version ", packageVersion("edgeR"))
cat(version_info, file=version.file)

images <- c("meanVariancePlot.png", "MAplot.png", "volcanoPlot.png", "pvalueHist.png")

html.content <- "<html><head><title></title></head><body>\n"

# include filter statistics
load(paste0(shared.dir, "/filterStats.RData"))
html.content <- paste0(html.content, "<h3>Filter Statistics:</h3><p>", num.prefiltered, " rows have been removed by pre-filtering.</p>\n")

# include images into HTML report (if the respective files exist)
for (image in images) {
 	if (file.exists(paste0(shared.dir, "/html/", image))) {
 		html.content <- paste0(html.content, "<img src=\"", image, "\" </img>\n")
 	}
}
html.content <- paste0(html.content, "</body></html>\n")

cat(html.content, file=index.file)
